package com.hendisantika.formvalidation.model;

/**
 * Created by IntelliJ IDEA.
 * Project : form-validation
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/07/18
 * Time: 07.12
 * To change this template use File | Settings | File Templates.
 */
public class Gender {
    public static final String MALE = "M";
    public static final String FEMALE = "F";
}
