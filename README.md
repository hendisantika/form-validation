# Form Validation

To run this project open your terminal then write or copy paste this command :
`mvn clean spring-boot:run`

Home page

![Home page](img/home.png "Home Page")

Members Page

![Members Page](img/members.png "Members page")

Validation Page

![Validation Page](img/validation.png "Validation page")

Success Page

![Success Page](img/Success.png "Success page")

List Members Page

![List Members Page](img/list.png "List page")


![List Members Page](gif/form-validation.gif "List page")
